package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            em.persist(parentEntity);
            parentId.setValue(parentEntity.getId());
            ChildEntity child = new ChildEntity("child");
            em.persist(child);
            childId.setValue(child.getId());
        });

        flushAndClear(em -> {
            ChildEntity children = em.find(ChildEntity.class, childId.getValue());
            children.setParentEntity(em.find(ParentEntity.class, parentId.getValue()));
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            assertEquals(parentId.getValue(), childEntity.getParentEntity().getId());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            ChildEntity child = new ChildEntity("child");
            em.persist(parent);
            parentId.setValue(parent.getId());
            em.persist(child);
            childId.setValue(child.getId());
            child.setParentEntity(parent);
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            assertEquals(parentId.getValue(), childEntity.getParentEntity().getId());
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            childEntity.setParentEntity(null);
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            assertNull(childEntity.getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            ChildEntity child = new ChildEntity("child");
            em.persist(parent);
            parentId.setValue(parent.getId());
            em.persist(child);
            childId.setValue(child.getId());
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            em.remove(childEntity);
            ParentEntity parentEntity = em.find(ParentEntity.class, parentId.getValue());
            em.remove(parentEntity);
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, childId.getValue());
            ParentEntity parentEntity = em.find(ParentEntity.class, parentId.getValue());
            assertNull(childEntity);
            assertNull(parentEntity);
        });
        // --end-->
    }
}